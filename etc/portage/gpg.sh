#!/bin/bash
 
KEYSERVER="hkps://keys.openpgp.org"
KEY="0xECAD322B56177724"
 
GPG_DIR="/etc/portage/gnupg"
 
PASS="$(openssl rand -base64 32)"
 
KEY_CONFIG_FILE="$(mktemp)"
chmod 600 "${KEY_CONFIG_FILE}"
 
export GNUPGHOME="${GPG_DIR}"
cat > "${KEY_CONFIG_FILE}" <<EOF
     %echo Generating Portage local OpenPGP trust key
     Key-Type: default
     Subkey-Type: default
     Name-Real: Portage Local Trust Key
     Name-Comment: local signing only
     Name-Email: portage@localhost
     Expire-Date: 0
     Passphrase: ${PASS}
     %commit
     %echo done
EOF
 
mkdir -p "${GNUPGHOME}"
gpg --batch --generate-key "${KEY_CONFIG_FILE}"
rm -f "${KEY_CONFIG_FILE}"
 
touch "${GNUPGHOME}/pass"
chmod 600 "${GNUPGHOME}/pass"
echo "${PASS}" > "${GNUPGHOME}/pass"
 
gpg --keyserver "${KEYSERVER}" --recv-keys "${KEY}"
gpg --batch --yes --pinentry-mode loopback --passphrase "${PASS}" --sign-key "${KEY}" 
echo -e "5\ny\n" | gpg --command-fd 0 --edit-key "${KEY}" trust
 
chmod ugo+r "${GNUPGHOME}/trustdb.gpg"
